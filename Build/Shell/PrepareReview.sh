#!/usr/bin/env bash

directory=$(echo "$CI_COMMIT_REF_NAME" | tr '[:upper:]' '[:lower:]');

ssh -p"$DEPLOY_TARGET_PORT" "$DEPLOY_TARGET_USER@$DEPLOY_TARGET_HOST" "
	if [ ! -d \"/var/www/www1/vhosts/mtug/review/$directory/releases\" ]; then
		if [ ! -d \"/var/www/www1/vhosts/mtug/review/$directory/\" ]; then
			mkdir /var/www/www1/vhosts/mtug/review/$directory;
			chown -R $DEPLOY_TARGET_USER:customer /var/www/www1/vhosts/mtug/review/$directory;
			echo "created /var/www/www1/vhosts/mtug/review/$directory";
		else
			echo "/var/www/www1/vhosts/mtug/review/$directory already exists";
		fi
		cp -rap /var/www/www1/vhosts/mtug/review/.dummy/releases /var/www/www1/vhosts/mtug/review/$directory/releases;
		find /var/www/www1/vhosts/mtug/review/$directory/releases -type f -exec sed -i 's/###CI_COMMIT_REF_NAME###/$directory/g' {} +
		echo "copied /var/www/www1/vhosts/mtug/review/.dummy/releases to /var/www/www1/vhosts/mtug/review/$directory/releases";

		if [ ! -d \"/var/www/www1/vhosts/mtug/review/.files/typo3temp_$directory/\" ]; then
			cp -rap /var/www/www1/vhosts/mtug/review/.files/typo3temp_dummy /var/www/www1/vhosts/mtug/review/.files/typo3temp_$directory;
			echo "copied /var/www/www1/vhosts/mtug/review/.files/typo3temp_dummy to /var/www/www1/vhosts/mtug/review/.files/typo3temp_$directory"
			rm /var/www/www1/vhosts/mtug/review/$directory/releases/current/web/typo3temp;
			cd /var/www/www1/vhosts/mtug/review/$directory/releases/current/web/
			ln -s files/typo3temp_$directory typo3temp;
			echo "symlinked files/typo3temp_$directory as typo3temp in /var/www/www1/vhosts/mtug/review/$directory/releases/current/web/"
		else
			echo "/var/www/www1/vhosts/mtug/review/.files/typo3temp_$directory already exists and symlinked as typo3temp"
		fi
	else
		echo "/var/www/www1/vhosts/mtug/review/$directory already prepared"
	fi
"
