#!/bin/bash

function parseUrl {
	# extract the protocol
	proto="$(echo $1 | grep :// | sed -e's,^\(.*://\).*,\1,g')"
	# remove the protocol
	url=$(echo $1 | sed -e s,$proto,,g)
	# extract the user (if any)
	userpassw="$(echo $url | grep @ | cut -d@ -f1)"
	# extract the password (if any)
	user="$(echo $userpassw | grep : | cut -d: -f1)"
	# extract the password (if any)
	password="$(echo $userpassw | grep : | cut -d: -f2)"
	# extract the host
	host=$(echo $url | sed -e s,$userpassw@,,g | cut -d/ -f1)
	# extract the path (if any)
	path="$(echo $url | grep / | cut -d/ -f2-)"

	urlParts=($proto $user $password $host $path);
}

urlParts=();
parseUrl $SITEMAP_URL
exitCode=0
urlList=$(wget --quiet $(if [ -n ${urlParts[1]} ]; then echo "--http-user=${urlParts[1]}"; fi) $(if [ -n ${urlParts[2]} ]; then echo "--http-password=${urlParts[2]}"; fi) ${urlParts[0]}${urlParts[3]}/${urlParts[4]} --output-document - | egrep -o "${urlParts[0]}${urlParts[3]}[^<]+")
urlCounter=$(echo "$urlList" | grep -v '^#' | wc -w)
finishedTestsCounter=0
echo "URLs to test: $urlCounter";
echo ""
while IFS=$'\n' read urlToTest; do
	if curl "$urlToTest" $(if [ -n ${urlParts[1]} ]; then echo "-u ${urlParts[1]}:${urlParts[2]}"; fi) -sS -k -f -L -o /dev/null; then
		echo "$urlToTest is OK"
	else
		echo "$urlToTest is NOT OK"
		exitCode=1
	fi
done < <(echo "$urlList" | grep -v '^#')

exit $exitCode
